import { API } from "./Dominio";

var TOKEN = "";
export function Administrador() {
  return "Administrador";
}
export function Vendedor() {
  return "Vendedor";
}
export function Gestor() {
  return "Gestor";
}

export function guardarToken(token) {
  TOKEN = token;
  localStorage.setItem("token", TOKEN);
  configAxios();
}
export function quitarToken() {
  TOKEN = "";
  localStorage.removeItem("token");
  configAxios();
}

export function configAxios(content_type,id_empresa) {
  const axios = require("axios");
  const adapter = axios.create({
    baseURL: API,
    headers: {
      Authorization: `Bearer ${TOKEN}`,
      "content-type": content_type ? content_type : "",
      "idEmpresa": id_empresa ? id_empresa : ""
    },
  });
  return adapter;
}
