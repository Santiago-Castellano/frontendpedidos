import { Gestor, Administrador, Vendedor } from "./ServicioAuth";

export function obtenerRoles(vista) {
  switch (vista) {
    case inicio():
      return [Gestor(), Administrador(), Vendedor()];
    case sincronizacion():
      return [Gestor(), Administrador(), Vendedor()];
    case pedidos():
      return [Gestor(), Administrador(), Vendedor()];
    case clientes():
      return [Gestor(), Administrador(), Vendedor()];
    case productos():
      return [Gestor(), Administrador(), Vendedor()];
    case empresas():
        return [Administrador()];
    case usuarios():
      return [Administrador(),Gestor()]
    default:
      return [];
  }
}

export function usuarios() {
  return "/Usuarios";
}

export function inicio() {
  return "/Inicio";
}
export function sincronizacion() {
  return "/Sincronizacion";
}

export function pedidos() {
  return "/Pedidos";
}
export function clientes() {
  return "/Clientes";
}

export function productos() {
  return "/Productos";
}

export function login() {
  return "/Login";
}
export function registro() {
  return "/Registro";
}
export function empresas() {
  return "/Empresas";
}
