export function obtenerErrores(mensaje, campos) {
  let lista = [];
  campos.forEach((c) => {
      if (mensaje[c]) {
          mensaje[c].forEach((m) => {
            lista.push({
              mensaje: m,
              tipo: "error",
              campo: c
            });
          });
      }
  });
  return lista;
}


export function armarMensaje(mensaje,tipo) {
  return {
    mensaje:mensaje,
    tipo:tipo,
    campo:""
  }
}