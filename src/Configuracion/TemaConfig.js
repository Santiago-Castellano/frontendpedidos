import { createMuiTheme } from "@material-ui/core/styles";

const Tema = createMuiTheme({
    palette: {
        primary: {
          light: '#41AB62',
          main: '#1FA64A',
          dark: '#2F613E',
          contrastText: '#fff',
        },
        secondary: {
          light: '#ff7961',
          main: '#f44336',
          dark: '#ba000d',
          contrastText: '#000',
        },
      },
});

export default Tema;
