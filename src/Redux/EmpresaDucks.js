import qs from "qs";
import { configAxios } from "../Configuracion/ServicioAuth";
import { armarMensaje } from "../Configuracion/Errores";

//constantes
const dataInicial = {
  array: [],
  error: []
};

const OBTENER_EMPRESA_EXITO = "OBTENER_EMPRESA_EXITO";
const AGREGAR_EMPRESA_EXITO = "AGREGAR_EMPRESA_EXITO";
const EDITAR_EMPRESA_EXITO = "EDITAR_EMPRESA_EXITO";
const BORRAR_EMPRESA_EXITO = "BORRAR_EMPRESA_EXITO";
const ERROR_E ="ERROR_E"
const BORRAR_ERROR_E ="BORRAR_ERROR_E"
//reducer
export default function empresaReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_EMPRESA_EXITO:
      return { ...state, array: action.payload 
      };
    case AGREGAR_EMPRESA_EXITO:
      return { ...state, array: [...state.array, action.payload],
        error: [armarMensaje("Se Agrego Correctamente", "success")],
       };
    case EDITAR_EMPRESA_EXITO:
      return {
        ...state,
        array: [
          ...state.array.filter((element) => element.Id !== action.payload.Id),
          action.payload,
        ],
        error: [armarMensaje("Se Edito Correctamente", "success")],
        
      };
    case BORRAR_EMPRESA_EXITO:
      return {
        ...state,
        array: state.array.filter((element) => element.Id !== action.payload),
        error: [armarMensaje("Se Borro Correctamente", "success")],
      
      };
    case ERROR_E:
      return { ...state, error: action.payload };
    case BORRAR_ERROR_E:
      return {
        ...state,
        error: state.error.filter(
          (element) => element.mensaje !== action.payload
        ),
      };
    default:
      return state;
  }
}

//acciones

export const obtenerEmpresasAccion = () => async (dispatch, getState) => {
  try {
    const axios_auth = configAxios();
    const res = await axios_auth.get("api/Empresa/get");
    dispatch({
      type: OBTENER_EMPRESA_EXITO,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type:ERROR_E,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })
  }
};

export const agregarEmpresaAccion = (data) => async (dispatch, getState) => {
  try {
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    const datos = qs.stringify({
      Id: 0,
      Nombre: data.Nombre,
    });
    const res = await axios_auth.post("api/Empresa/Agregar", datos);

    dispatch({
      type: AGREGAR_EMPRESA_EXITO,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type:ERROR_E,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })
  }
};

export const borrarEmpresaAccion = (data) => async (dispatch, getState) => {
  try {
    const axios_auth = configAxios();
    await axios_auth.post(`api/Empresa/Borrar?id=${data.Id}`);
    dispatch({
      type: BORRAR_EMPRESA_EXITO,
      payload: data.Id,
    });
  } catch (error) {
    dispatch({
      type:ERROR_E,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })  
  }
};

export const editarEmpresaAccion = (data) => async (dispatch, getState) => {
  try {
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    const datos = qs.stringify({
      Id: data.Id,
      Nombre: data.Nombre,
    });
    const res = await axios_auth.post("api/Empresa/Editar", datos);
    dispatch({
      type: EDITAR_EMPRESA_EXITO,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type:ERROR_E,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })  
  }
};


export const borrarErrorAccion = (mensaje) => async (dispatch) => {
  dispatch({
    type: BORRAR_ERROR_E,
    payload: mensaje,
  });
};
