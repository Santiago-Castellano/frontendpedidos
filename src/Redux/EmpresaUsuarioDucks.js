import { configAxios, Administrador } from "../Configuracion/ServicioAuth";
import qs from "qs";
import { armarMensaje } from "../Configuracion/Errores";
//constantes
const dataInicial = {
  array: [],
  error: []
};

const OBTENER_USUARIOS_EXITO = "OBTENER_USUARIOS_EXITO";
const DESVINCULAR_USUARIO_EXITO = "DESVINCULAR_USUARIO_EXITO";
const VINCULAR_USUARIO_EXITO = "VINCULAR_USUARIO_EXITO";
const ERROR_EU = "ERROR_EU";
const BORRAR_ERROR_EU = "BORRAR_ERROR_EU";

//reducer
export default function empresaUsuarioReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_USUARIOS_EXITO:
      return { ...state, array: action.payload };
    case DESVINCULAR_USUARIO_EXITO:
      return {
        ...state,
        array: state.array.filter(
          (element) => element.Email !== action.payload
        ),
        error: [armarMensaje("Se desvinculo Correctamente", "success")],
      };
    case VINCULAR_USUARIO_EXITO:
      return { ...state, array: [...state.array, action.payload],
        error: [armarMensaje("Se Vinculo Correctamente", "success")],
       };
    case ERROR_EU:
      return { ...state, error: action.payload };
    case BORRAR_ERROR_EU:
      return {
        ...state,
        error: state.error.filter(
          (element) => element.mensaje !== action.payload
        ),
      };
    default:
      return state;
  }
}

//acciones
export const obtenerUsuariosAccion = (idEmpresa) => async (
  dispatch,
  getState
) => {
  try {
    const estado = getState();
    idEmpresa = idEmpresa != null ? idEmpresa : estado.usuario.empresas.Id;
    const axios_auth = configAxios(null, null);
    const res = await axios_auth.get(
      `api/Empresa/ObtenerUsuariosEmpresa?idEmpresa=${idEmpresa}`
    );
    dispatch({
      type: OBTENER_USUARIOS_EXITO,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type:ERROR_EU,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })
  }
};

export const desvincularUsuarioAccion = (data, idEmpresa) => async (
  dispatch,
  getState
) => {
  try {
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    const estado = getState();
    const datos = qs.stringify({
      idEmpresa:
        estado.usuario.rol === Administrador()
          ? idEmpresa
          : estado.usuario.empresas.Id,
      Email: data.Email,
    });
    await axios_auth.post("api/Empresa/DesvincularUsuario", datos);
    dispatch({
      type: DESVINCULAR_USUARIO_EXITO,
      payload: data.Email,
    });
  } catch (error) {
    dispatch({
      type:ERROR_EU,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })
  }
};

export const vincularUsuarioAccion = (data, idEmpresa) => async (
  dispatch,
  getState
) => {
  try {
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    const estado = getState();
    const datos = qs.stringify({
      idEmpresa:
        estado.usuario.rol === Administrador()
          ? idEmpresa
          : estado.usuario.empresas.Id,
      Email: data.Email,
      Rol: data.Rol,
      NroVendedor: data.NroVendedor
    });
    await axios_auth.post("api/Empresa/VincularUsuario", datos);
    dispatch({
      type: VINCULAR_USUARIO_EXITO,
      payload: {
        Email: data.Email,
        Rol: data.Rol,
      },
    });
  } catch (error) {
    dispatch({
      type:ERROR_EU,
      payload:[armarMensaje(error.response.data.Message,"error")]
    })
  }
};

export const borrarErrorAccion = (mensaje) => async (dispatch) => {
  dispatch({
    type: BORRAR_ERROR_EU,
    payload: mensaje,
  });
};
