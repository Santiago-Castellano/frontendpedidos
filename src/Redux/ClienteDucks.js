import { configAxios } from '../Configuracion/ServicioAuth';
//constantes
const dataInicial = {
  array: [],
};

const OBTENER_CLIENTES_EXITO = "OBTENER_CLIENTES_EXITO";
//reducer
export default function clienteReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_CLIENTES_EXITO:
      return { ...state, array: action.payload };
    default:
      return state;
  }
}

//acciones

export const obtenerClientesAccion = () => async (dispatch, getState) => {
  try {
    const estado = getState();
    const axios_auth = configAxios(null,estado.usuario.empresas.Id);
    const res = await axios_auth.get("api/Cliente/get");
    dispatch({
      type: OBTENER_CLIENTES_EXITO,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};
