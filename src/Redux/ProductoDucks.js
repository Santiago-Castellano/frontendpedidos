import { configAxios } from "../Configuracion/ServicioAuth";

//constantes
const dataInicial = {
  array: [],
  lineas: [],
};

const OBTENER_PRODUCTOS_EXITO = "OBTENER_PRODUCTOS_EXITO";
const OBTENER_LINEAS_EXITO = "OBTENER_LINEAS_EXITO";
//reducer
export default function productoReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_PRODUCTOS_EXITO:
      return { ...state, array: action.payload };
    case OBTENER_LINEAS_EXITO:
      return { ...state, lineas: action.payload };
    default:
      return state;
  }
}

//acciones

export const obtenerProductoAccion = (idCliente) => async (
  dispatch,
  getState
) => {
  try {
    const estado = getState();
    const axios_auth = configAxios(null, estado.usuario.empresas.Id);
    let codCliente = null;
    if (idCliente) {
      const cliente = estado.clientes.array.filter((c) => c.Id === idCliente)[0];
      codCliente = cliente.Codigo;
    }
    const res = await axios_auth.get(`api/Producto/get?codigoCliente=${codCliente}`);
    dispatch({
      type: OBTENER_PRODUCTOS_EXITO,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};

export const obtenerLineasAccion = () => async (dispatch, getState) => {
  try {
    const estado = getState();
    const axios_auth = configAxios(null, estado.usuario.empresas.Id);
    const res = await axios_auth.get("api/Producto/getLineas");
    dispatch({
      type: OBTENER_LINEAS_EXITO,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};
