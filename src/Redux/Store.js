import { createStore, combineReducers, compose, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { leerUsuarioAccion } from './UsuarioDucks';

import clienteReducer from "./ClienteDucks";
import usuarioReducer from "./UsuarioDucks";
import sincronizacionReducer from './SincronizacionDucks';
import empresaReducer from './EmpresaDucks';
import empresaUsuarioReducer from './EmpresaUsuarioDucks';
import productoReducer from './ProductoDucks';
import pedidoReducer from "./PedidoDucks";


const rootReducer = combineReducers({
  clientes: clienteReducer,
  usuario: usuarioReducer,
  sincronizacion:sincronizacionReducer,
  empresas:empresaReducer,
  empresaUsuario: empresaUsuarioReducer,
  producto:productoReducer,
  pedido: pedidoReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function generateStore() {
  const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
  );
  leerUsuarioAccion()(store.dispatch);
  return store;
}
