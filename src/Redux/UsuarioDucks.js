import axios from "axios";
import obtenerUrl from "../Configuracion/Dominio";
import { Administrador } from "../Configuracion/ServicioAuth";
import qs from "qs";
import {
  guardarToken,
  configAxios,
  quitarToken,
} from "../Configuracion/ServicioAuth";
import { obtenerErrores, armarMensaje } from "../Configuracion/Errores";
//constantes
const dataInicial = {
  token: "",
  logeado: false,
  rol: "",
  nombre: "",
  empresas: [],
  cargando:true,
  error: [],
};

const SELECCIONAR_EMPRESA_EXITO = "SELECCIONAR_EMPRESA_EXITO";
const INICIAR_SESION = "INICIAR_SESION";
const CERRAR_SESION = "CERRAR_SESION";
const REGISTRO = "REGISTRO";
const ERROR_USUARIO = "ERROR_USUARIO";
const BORRAR_ERROR_USUARIO = "BORRAR_ERROR_USUARIO";
const SET_CARGANDO_LOGIN ="SET_CARGANDO_LOGIN"
//reducer
export default function usuarioReducer(state = dataInicial, action) {
  switch (action.type) {
    case INICIAR_SESION:
      return {
        ...state,
        token: action.payload.token,
        logeado: action.payload.logeado,
        rol: action.payload.rol,
        empresas: action.payload.empresas,
        nombre:action.payload.nombre,
        cargando:false,
        error: [],
      };
    case CERRAR_SESION:
      return { ...dataInicial,cargando:false };
    case SET_CARGANDO_LOGIN:
      return { ...state,cargando:action.payload };
    case SELECCIONAR_EMPRESA_EXITO:
      return { ...state, empresas: action.payload };
    case REGISTRO:
      return {
        ...dataInicial,
        error: [
          armarMensaje(
            "Se registro correctamente, solicite permiso con su empresa",
            "success"
          ),
        ],
      };
    case ERROR_USUARIO:
      return { ...state, error: action.payload,cargando:false };
    case BORRAR_ERROR_USUARIO:
      return {
        ...state,
        error: state.error.filter(
          (element) => element.mensaje !== action.payload
        ),
      };
    default:
      return state;
  }
}

//acciones
export const iniciarSesionAccion = (username, password) => async (dispatch) => {
  try {
    const url = obtenerUrl("/Token");
    const datos = qs.stringify({
      username: username,
      password: password,
      grant_type: "password",
    });

    const res = await axios.post(url, datos, {
      headers: { "content-type": "application/x-www-form-urlencoded" },
    });
    guardarToken(res.data.access_token);
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    const res_2 = await axios_auth.get("api/Account/UserInfo");
    let empresas = [];
    if (res_2.data.Rol !== Administrador()) {
      const res_empresa = await axios_auth.get("api/Empresa/get");
      if (res_empresa.data.length === 1) {
        empresas = res_empresa.data[0];
      } else {
        empresas = res_empresa.data;
      }
    }
    dispatch({
      type: INICIAR_SESION,
      payload: {
        token: res.data.access_token,
        logeado: true,
        rol: res_2.data.Rol,
        empresas: empresas,
        nombre: res_2.data.Email

      },
    });
  } catch (error) {
    console.log(error);
    let mensaje = error.response ? error.response.data.error_description : "Ocurrio un problema, Intene Nuevamente."; 
    dispatch({
      type: ERROR_USUARIO,
      payload: [
        {
          mensaje: mensaje,
          tipo: "error",
          campo: "",
        },
      ],
    });
  }
};

export const registroAccion = (
  username,
  password,
  confirmPassword,
  email
) => async (dispatch) => {
  try {
    const url = obtenerUrl("/Account/Register");
    const datos = qs.stringify({
      Email: email,
      Username: username,
      Password: password,
      ConfirmPassword: confirmPassword,
    });
    await axios.post(url, datos);
    dispatch({
      type: REGISTRO,
    });
  } catch (error) {
    let mensaje = error.response.data.ModelState;
    let lista = obtenerErrores(mensaje, [
      "model.Email",
      "model.Username",
      "model.Password",
      "",
      "model.ConfirmPassword",
    ]);
    dispatch({
      type: ERROR_USUARIO,
      payload: lista,
    });
  }
};

export const cerrarSesionAccion = () => async (dispatch) => {
  try {
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    await axios_auth.post("api/Account/Logout");
    quitarToken();
    dispatch({
      type: CERRAR_SESION,
    });
  } catch (error) {
    console.log(error);
  }
};

export const leerUsuarioAccion = () => async (dispatch) => {
  if (localStorage.getItem("token")) {
    try {
      const token = localStorage.getItem("token");
      guardarToken(token);
      const axios_auth = configAxios("application/x-www-form-urlencoded");
      const res = await axios_auth.get("api/Account/UserInfo");
      let empresas = [];
      if (res.data.Rol !== Administrador()) {
        const res_empresa = await axios_auth.get("api/Empresa/get");
        if (res_empresa.data.length === 1) {
          empresas = res_empresa.data[0];
        } else {
          empresas = res_empresa.data;
        }
      }
      dispatch({
        type: INICIAR_SESION,
        payload: {
          token: token,
          logeado: true,
          rol: res.data.Rol,
          empresas: empresas,
          nombre: res.data.Email
        },
      });
    } catch (error) {
      console.log(error);
      dispatch({
        type: CERRAR_SESION,
      });
    }
  } else {
    dispatch({
      type: CERRAR_SESION,
    });
  }
};

export const seleccionarEmpresaAccion = (empresa) => async (dispatch) => {
  dispatch({
    type: SELECCIONAR_EMPRESA_EXITO,
    payload: empresa,
  });
};

export const borrarErrorAccion = (mensaje) => async (dispatch) => {
  dispatch({
    type: BORRAR_ERROR_USUARIO,
    payload: mensaje,
  });
};


export const cargandoSitioAccion = (cargando) => async (dispatch) => {
  dispatch({
    type: SET_CARGANDO_LOGIN,
    payload:cargando
  });
};
