import qs from "qs";
import { configAxios } from "../Configuracion/ServicioAuth";
import { armarMensaje } from "../Configuracion/Errores";
import { cargandoSitioAccion } from "./UsuarioDucks";

//constantes
const dataInicial = {
  array: [],
  error: [],
};

const OBTENER_PEDIDO_EXITO = "OBTENER_PEDIDO_EXITO";
const AGREGAR_PEDIDO_EXITO = "AGREGAR_PEDIDO_EXITO";
const ERROR_PEDIDO = "ERROR_PEDIDO";
const BORRAR_ERROR_PEDIDO = "BORRAR_ERROR_PEDIDO";

export default function pedidoReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_PEDIDO_EXITO:
      return { ...state, array: action.payload };
    case AGREGAR_PEDIDO_EXITO:
      return {
        ...state,
        array: [...state.array, action.payload],
        error: [armarMensaje("Se Agrego Correctamente", "success")],
      };
    case ERROR_PEDIDO:
      return { ...state, error: action.payload };
    case BORRAR_ERROR_PEDIDO:
      return {
        ...state,
        error: state.error.filter(
          (element) => element.mensaje !== action.payload
        ),
      };
    default:
      return state;
  }
}

//acciones
export const agregarPedidoAccion = (cabecera, carrito) => async (
  dispatch,
  getState
) => {
  try {
    const estado = getState();
    const axios_auth = configAxios("application/x-www-form-urlencoded");
    const datos = qs.stringify({
        Id:cabecera.Id,
        IdEmpresa: estado.usuario.empresas.Id,
        IdCliente:cabecera.cliente.Id,
        RazonSocial:cabecera.cliente.RazonSocial,
        Cuit: cabecera.cliente.Cuit,   
        SituacionIva:cabecera.cliente.SituacionIva,
        ObservacionCliente: cabecera.cliente.Observacion,
        Observaciones: cabecera.observacion,
        Detalles: carrito
    });
    let accion = cabecera.Id === 0 ? "Agregar" : "Editar";
    const res = await axios_auth.post(`api/Pedido/${accion}`, datos);

    dispatch({
      type: AGREGAR_PEDIDO_EXITO,
      payload: res.data,
    });
    dispatch(cargandoSitioAccion(false));

  } catch (error) {
    dispatch(cargandoSitioAccion(false));
    dispatch({
      type: ERROR_PEDIDO,
      payload: [armarMensaje(error.response.data.Message, "error")],
    });
  }
};

export const obtenerPedidosAccion = () => async (dispatch, getState) => {
  try {
    const estado = getState();
    const axios_auth = configAxios(null,estado.usuario.empresas.Id);
    const res = await axios_auth.get("api/Pedido/get");
    dispatch({
      type: OBTENER_PEDIDO_EXITO,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};



export const borrarErrorAccion = (mensaje) => async (dispatch) => {
  dispatch({
    type: BORRAR_ERROR_PEDIDO,
    payload: mensaje,
  });
};
