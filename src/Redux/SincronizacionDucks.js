import { configAxios } from "../Configuracion/ServicioAuth";
import { armarMensaje } from "../Configuracion/Errores";
//constantes
const dataInicial = {
  array: [],
  error: [],
  cargando: false,
};

const IMPORTAR_EXITO = "IMPORTAR_EXITO";
const OBTENER_DATOS = "OBTENER_DATOS";
const SET_CARGANDO = "SET_CARGANDO";
const BORRAR_ERROR_SINC = "BORRAR_ERROR_SINC";
const ERROR_SINC = "ERROR_SINC";
//reducer
export default function sincronizacionReducer(state = dataInicial, action) {
  switch (action.type) {
    case OBTENER_DATOS:
      return {
        ...state,
        array: action.payload,
      };
    case SET_CARGANDO:
      return {
        ...state,
        array: [
          ...state.array.filter(
            (e) => e.NombreModelo !== action.payload.NombreModelo
          ),
          action.payload,
        ],
      };
    case IMPORTAR_EXITO:
      return {
        ...state,
        error: [{ mensaje: action.payload.mensaje, tipo: "success" }],
        array: action.payload.data,
        cargando: false,
      };
    case ERROR_SINC:
      return { ...state, error: action.payload, cargando: false };
    case BORRAR_ERROR_SINC:
      return {
        ...state,
        error: state.error.filter(
          (element) => element.mensaje !== action.payload
        ),
        cargando: false,
      };
    default:
      return state;
  }
}

//acciones
export const obtenerDatosAccion = () => async (dispatch, getState) => {
  try {
    const estado = getState();
    const axios_auth = configAxios(null, estado.usuario.empresas.Id);
    const res = await axios_auth.get("api/Sincronizacion/get");

    //const res = await axios_auth.get("api/Cliente");
    dispatch({
      type: OBTENER_DATOS,
      payload: res.data,
    });
  } catch (error) {
    console.log(error);
  }
};
export const cargarndoAccion = (modelo, importacion) => async (
  dispatch,
  getState
) => {
  try {
    const estado = getState();
    const item = estado.sincronizacion.array.filter(
      (e) => e.NombreModelo === modelo
    )[0];
    if (importacion) {
      item.Importacion = "Importando";
    } else {
      item.Exportacion = "Exportando";
    }
    dispatch({
      type: SET_CARGANDO,
      payload: item,
    });
  } catch (error) {
    console.log(error);
  }
};

export const importarAccion = (form, modelo) => async (dispatch, getState) => {
  try {
    const estado = getState();
    const axios = configAxios(
      "multipart/form-data",
      estado.usuario.empresas.Id
    );
    await axios.post(`api/${modelo}/Importar`, form);

    const axios_auth = configAxios(null, estado.usuario.empresas.Id);
    const res = await axios_auth.get("api/Sincronizacion/get");

    dispatch({
      type: IMPORTAR_EXITO,
      payload: {
        mensaje: `${modelo} Importados Correctamente`,
        data: res.data,
      },
    });
  } catch (error) {
    let mensaje =
      error.response.data.ExceptionMessage || error.response.data.Message;
    dispatch({
      type: ERROR_SINC,
      payload: [armarMensaje(mensaje, "error")],
    });
  }
};

export const exportarAccion = (modelo) => async (dispatch, getState) => {
  try {
    const estado = getState();
    const axios = configAxios(null, estado.usuario.empresas.Id);
    axios({
      url: `api/${modelo}/Exportar`,
      method: "GET",
      responseType: "blob",
    }).then((response) => {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Pedidos.csv"); 
      document.body.appendChild(link);
      link.click();
    });

    const res = await axios.get("api/Sincronizacion/get");

    dispatch({
      type: IMPORTAR_EXITO,
      payload: {
        mensaje: `${modelo} Exportado Correctamente`,
        data: res.data,
      },
    });
  } catch (error) {
    console.log(error);
  }
};

export const borrarErrorAccion = (mensaje) => async (dispatch) => {
  dispatch({
    type: BORRAR_ERROR_SINC,
    payload: mensaje,
  });
};
