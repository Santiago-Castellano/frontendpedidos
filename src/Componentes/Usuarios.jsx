import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  obtenerUsuariosAccion,
  desvincularUsuarioAccion,
  vincularUsuarioAccion,
  borrarErrorAccion,
} from "../Redux/EmpresaUsuarioDucks";

import { obtenerEmpresasAccion } from "../Redux/EmpresaDucks";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import Typography from "@material-ui/core/Typography";
import { Administrador } from "../Configuracion/ServicioAuth";
import { useState } from "react";
import Alerta from "./Layout/Alerta";
import Tabla from "./Layout/Tabla";

const Usuarios = () => {
  const dispatch = useDispatch();
  const datos = useSelector((store) => store.empresaUsuario.array);
  const empresas = useSelector((store) => store.empresas.array);
  const usuario = useSelector((store) => store.usuario);
  const error = useSelector((store) => store.empresaUsuario.error);
  const [empresa, setEmpresa] = useState(null);
  const columnas = [
    { title: "Email", field: "Email" },
    { title: "Nro. Vendedor", field: "NroVendedor" },
    {
      title: "Rol",
      field: "Rol",
      lookup: { Gestor: "Gestor", Vendedor: "Vendedor" },
    },
  ];

  React.useEffect(() => {
    const obtenerInfo = () => {
      if (usuario.rol === Administrador()) {
        dispatch(obtenerEmpresasAccion());
      } else {
        dispatch(obtenerUsuariosAccion(null));
      }
    };
    obtenerInfo();
  }, [dispatch, usuario.rol]);

  const handleRowAdd = (newData, resolve) => {
    const vincular = () => {
      dispatch(vincularUsuarioAccion(newData, empresa));
    };
    vincular();
    resolve();
  };
  const handleRowDelete = (oldData, resolve) => {
    const desvincular = () => {
      dispatch(desvincularUsuarioAccion(oldData, empresa));
    };
    desvincular();
    resolve();
  };
  const handleChange = (event) => {
    setEmpresa(event.target.value);
    dispatch(obtenerUsuariosAccion(event.target.value));
  };
  return (
    <div>
      {error.length > 0
        ? error.map((e) => (
            <Alerta
              key={e.mensaje}
              mensaje={e.mensaje}
              tipo={e.tipo}
              accion={borrarErrorAccion}
            />
          ))
        : null}
      <Typography variant="h4">Usuarios</Typography>
      {usuario.rol === Administrador() ? (
        <>
          <Typography variant="h6"> Seleccionar Empresa</Typography>
          <Select onChange={handleChange}>
            {empresas.map((item) => (
              <MenuItem key={item.Id} value={item.Id.toString()}>
                {item.Nombre}
              </MenuItem>
            ))}
          </Select>
        </>
      ) : null}

      <Tabla
        seleccionColumnas={true}
        title={""}
        columns={columnas}
        see={columnas}
        data={datos}
        editable={{
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              handleRowAdd(newData, resolve);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              handleRowDelete(oldData, resolve);
            }),
        }}
      />
    </div>
  );
};

export default Usuarios;
