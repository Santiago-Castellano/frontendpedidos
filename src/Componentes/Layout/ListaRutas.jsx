import React from "react";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import DashboardIcon from "@material-ui/icons/Dashboard";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import Divider from "@material-ui/core/Divider";
import PeopleIcon from "@material-ui/icons/People";
import LabelIcon from "@material-ui/icons/Label";
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import SyncIcon from "@material-ui/icons/Sync";
import BusinessIcon from "@material-ui/icons/Business";
import List from "@material-ui/core/List";
import { withRouter } from "react-router-dom";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { cerrarSesionAccion } from "../../Redux/UsuarioDucks";

import {
  inicio,
  sincronizacion,
  productos,
  pedidos,
  clientes,
  login,
  empresas,
  obtenerRoles,
  usuarios,
} from "../../Configuracion/Rutas";

const ListaRutas = (props) => {
  const dispatch = useDispatch();
  const usuario = useSelector((store) => store.usuario);
  const cerrar = () => {
    dispatch(cerrarSesionAccion());
    props.history.push(login());
  };
  return (
    <>
      <List>
        {obtenerRoles(inicio()).includes(usuario.rol) ? (
          <NavLink
            style={{ textDecoration: "none", color: "black" }}
            to={inicio()}
          >
            <ListItem button>
              <ListItemIcon>
                <DashboardIcon />
              </ListItemIcon>
              <ListItemText primary="Inicio" />
            </ListItem>
          </NavLink>
        ) : null}
        {obtenerRoles(pedidos()).includes(usuario.rol) ? (
          <NavLink
            style={{ textDecoration: "none", color: "black" }}
            to={pedidos()}
          >
            <ListItem button>
              <ListItemIcon>
                <ShoppingCartIcon />
              </ListItemIcon>
              <ListItemText primary="Pedidos" />
            </ListItem>
          </NavLink>
        ) : null}
        {obtenerRoles(clientes()).includes(usuario.rol) ? (
          <NavLink
            style={{ textDecoration: "none", color: "black" }}
            to={clientes()}
          >
            <ListItem button>
              <ListItemIcon>
                <PeopleIcon />
              </ListItemIcon>
              <ListItemText primary="Clientes" />
            </ListItem>
          </NavLink>
        ) : null}
        {obtenerRoles(productos()).includes(usuario.rol) ? (
          <NavLink
            style={{ textDecoration: "none", color: "black" }}
            to={productos()}
          >
            <ListItem button>
              <ListItemIcon>
                <LabelIcon />
              </ListItemIcon>
              <ListItemText primary="Productos" />
            </ListItem>
          </NavLink>
        ) : null}
        {obtenerRoles(sincronizacion()).includes(usuario.rol) ? (
          <NavLink
            style={{ textDecoration: "none", color: "black" }}
            to={sincronizacion()}
          >
            <ListItem button>
              <ListItemIcon>
                <SyncIcon />
              </ListItemIcon>
              <ListItemText primary="Sincronizacion" />
            </ListItem>
          </NavLink>
        ) : null}
      </List>
      <Divider />
      <List>
        {obtenerRoles(inicio()).includes(usuario.rol) ? (
          <>
            <ListSubheader inset>Cuenta</ListSubheader>
          
            <ListItem
              button
              onClick={() => {
                cerrar();
              }}
            >
              <ListItemIcon>
                <ExitToAppIcon />
              </ListItemIcon>
              <ListItemText primary="Cerrar Sesion" />
            </ListItem>
          </>
        ) : null}
      </List>
      {obtenerRoles(usuarios()).includes(usuario.rol) ? (
        <>
          <List>
            <ListSubheader inset>Administracion</ListSubheader>
            <NavLink
                style={{ textDecoration: "none", color: "black" }}
                to={usuarios()}
              >
                <ListItem button>
                  <ListItemIcon>
                    <PersonAddIcon />
                  </ListItemIcon>
                  <ListItemText primary="Usuarios" />
                </ListItem>
              </NavLink>
            {obtenerRoles(empresas()).includes(usuario.rol) ? (
              <NavLink
                style={{ textDecoration: "none", color: "black" }}
                to={empresas()}
              >
                <ListItem button>
                  <ListItemIcon>
                    <BusinessIcon />
                  </ListItemIcon>
                  <ListItemText primary="Empresas" />
                </ListItem>
              </NavLink>
            ) : null}
          </List>
        </>
      ) : null}
    </>
  );
};

export default withRouter(ListaRutas);
