import React from "react";
import MaterialTable from "material-table";
import { TablaIcons } from "./TablaIcons";
import ListaSeleccion from "./ListaSeleccion";
import { useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";

// Configuración del lenguaje de la tabla.
const localization = {
  body: {
    emptyDataSourceMessage: "No hay registros para mostrar",
    addTooltip: "Agregar",
    deleteTooltip: "Eliminar",
    editTooltip: "Editar",
    filterRow: {
      filterPlaceHolder: "Filtrar",
      filterTooltip: "Filtrar",
    },
    editRow: {
      deleteText: "Seguro quiere eliminar este registro?",
      cancelTooltip: "Cancelar",
      saveTooltip: "Guardar",
    },
  },
  grouping: {
    placeholder: "Arrastrar columnas aquí para agrupar.",
    groupedBy: "Agrupado por:",
  },
  header: {
    actions: "Acciones",
  },
  pagination: {
    labelRowsSelect: "filas",
    labelDisplayedRows: "{from}-{to} de {count}",
    labelRowsPerPage: "Filas por página",
    firstAriaLabel: "Primera",
    firstTooltip: "Primera",
    previousAriaLabel: "Página anterior",
    previousTooltip: "Página anterior",
    nextAriaLabel: "Página siguiente",
    nextTooltip: "Página siguiente",
    lastAriaLabel: "Última",
    lastTooltip: "Última",
  },
  toolbar: {
    nRowsSelected: "{0} fila(s) seleccionadas",
    searchTooltip: "Buscar",
    searchPlaceholder: "Buscar",
    addRemoveColumns: "Agregar o eliminar columnas",
    showColumnsTitle: "Mostrar columnas",
    showColumnsAriaLabel: "Mostrar columnas",
    exportTitle: "Exportar",
    exportAriaLabel: "Exportar",
    exportName: "Exportar como CSV",
  },
};

export const Tabla = ({
  title,
  columns,
  see,
  data,
  editable,
  options,
  actions,
  detailPanel,
  seleccionColumnas,
  onRowClick,
}) => {
  const [open, setOpen] = React.useState(false);
  const [columnsSee, setColumnsSee] = useState(see);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (value) => {
    setOpen(false);
  };

  const defaultOptions = {
    actionsColumnIndex: -1, // Columna de acciones a la derecha.
    pageSize: 10,
    pageSizeOptions: [10, 20, 30, 50, 100],
    paginationType: "stepped", // Mostrar números de páginas.
    headerStyle: {
      position: "sticky",
      top: 0,
      backgroundColor: "#f5f5f5",
    },
    padding: "dense",
    emptyRowsWhenPaging: false, // No generar lineas vacías para rellenar.
    detailPanelType: "single", // Permitir que haya solo uno abierto.
  };

  const finalOptions = options
    ? Object.assign(options, defaultOptions)
    : defaultOptions;

  const UpdateColumnsSee = (newColums) => {
    const newSee = columns.filter((element) => {
      if (newColums.includes(element.field)) {
        return element;
      } else {
        return null;
      }
    });
    if (newSee.length === 0) {
      setColumnsSee(see);
    } else {
      setColumnsSee(newSee);
    }
  };

  return (
    <>
      {seleccionColumnas ? (
        <>
          <Button
            style={{ marginBottom: 10, marginTop: 10 }}
            variant="outlined"
            color="primary"
            onClick={handleClickOpen}
          >
            Seleccionar Columnas
          </Button>
          <Dialog onClose={handleClose} open={open}>
            <DialogTitle id="simple-dialog-title">
              Seleccion de Columnas
            </DialogTitle>
            <ListaSeleccion columns={columns} action={UpdateColumnsSee} />
            <Button variant="outlined" color="secondary" onClick={handleClose}>
              Cerrar
            </Button>
          </Dialog>{" "}
        </>
      ) : null}

      <MaterialTable
        onRowClick={(event, data) => (onRowClick ? onRowClick(data) : null)}
        title={title}
        columns={columnsSee}
        data={data}
        editable={editable}
        actions={actions}
        detailPanel={detailPanel}
        options={finalOptions}
        localization={localization}
        // Condiguración de los iconos de editable.
        icons={TablaIcons}
      />
    </>
  );
};

export default Tabla;
