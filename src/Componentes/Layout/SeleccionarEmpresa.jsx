import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import { seleccionarEmpresaAccion } from "../../Redux/UsuarioDucks";

const useStyles = makeStyles((theme) => ({
  box: {
    margin: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(1),
    height: 60
  },
}));

const SeleccionarEmpresa = () => {
  const classes = useStyles();
  const empresas = useSelector((store) => store.usuario.empresas);
  const dispatch = useDispatch();
  const seleccionarEmpresa = (empresa) => {
    const selectEmpresa = () => {
      dispatch(seleccionarEmpresaAccion(empresa));
    };
    selectEmpresa();
  };
  return (
    <div>
      <Typography variant="h4">Seleccionar Empresa</Typography>
        {empresas.map((item) => (
          <Box key={item.Id} className={classes.box}>
            <Button
              variant="outlined"
              color="primary"
              className={classes.button}
              startIcon={<ArrowForwardIosIcon />}
              onClick={() => seleccionarEmpresa(item)}
            >
              <Typography variant="h5" component="h2">
                {item.Nombre}
              </Typography>
            </Button>
          </Box>
        ))}
    </div>
  );
};

export default SeleccionarEmpresa;
