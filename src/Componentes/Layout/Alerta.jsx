import React from "react";
import Alert from "@material-ui/lab/Alert";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Collapse from "@material-ui/core/Collapse";
import { useDispatch } from "react-redux";

const Alerta = ({ mensaje, tipo, accion }) => {
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(true);
  const CerrarAlerta = () => {
    setOpen(false);
    if (accion) {
      dispatch(accion(mensaje));
    }
  };
  return (
    <Collapse in={open}>
      <Alert
        variant="outlined"
        action={
          <IconButton
            aria-label="close"
            color="inherit"
            size="small"
            onClick={CerrarAlerta}
          >
            <CloseIcon fontSize="inherit" />
          </IconButton>
        }
        severity={tipo}
      >
        {mensaje}
      </Alert>
    </Collapse>
  );
};

export default Alerta;
