import React from "react";
import Select from "react-select";
import { makeStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { useDispatch, useSelector } from "react-redux";
import { obtenerClientesAccion } from "../../Redux/ClienteDucks";
import TextField from "@material-ui/core/TextField";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles((theme) => ({
  texto: {
    width: "100%",
  },
  obs: {
    height:2,
  },
  check: {
    marginLeft: 1,
    paddingLeft: 1,
  },
}));

const SeleccionCliente = ({ accion, cliente }) => {
  const clases = useStyles();
  const dispatch = useDispatch();
  const clientes = useSelector((store) => store.clientes.array);
  const situacionIva = [
    { value: "Responsable Inscripto", label: "Responsable Inscripto" },
    { value: "Monotributo", label: "Monotributo" },
    { value: "Exento", label: "Exento" },
  ];
  const handleChange = (event) => {
    accion("", "", "","", event.target.checked, 0);
  };
  React.useEffect(() => {
    const obtenerInfo = () => {
      if (clientes.length === 0) {
        dispatch(obtenerClientesAccion());
      }
    };
    obtenerInfo();
  });

  return (
    <>
      <Box display="flex" m={0} p={1}>
        <FormControlLabel
          control={
            <Checkbox
              checked={cliente === null ? false : cliente.Nuevo}
              onChange={handleChange}
              name="nuevoCliente"
              color="primary"
              label="Nuevo Cliente"
            />
          }
          label="Nuevo Cliente"
        />
      </Box>
      {(cliente === null ? false : cliente.Nuevo) === false ? (
        <Box display="flex" m={1} p={1}>
          <Select
            className={clases.texto}
            onChange={(value) =>
              accion(value.Cuit, value.RazonSocial, "","", false, value.Id)
            }
            placeholder={"Seleccionar Cliente"}
            getOptionLabel={(option) =>
              `${option.Cuit} - ${option.RazonSocial}`
            }
            noOptionsMessage={() => {
              return "No Se Encontraron Resultados";
            }}
            options={clientes}
            getOptionValue={(option) => option["Id"]}
            defaultValue={
              cliente === null
                ? null
                : clientes.filter((e) => e.Id === cliente.Id)[0]
            }
          />
        </Box>
      ) : (
        <form autoComplete="off">
          <Box display="flex" justifyContent="center" m={1} p={1}>
            <TextField
              className={clases.texto}
              onChange={(e) => {
                accion(cliente.Cuit, e.target.value, cliente.SituacionIva,cliente.Observacion, cliente.Nuevo, 0);
              }}
              label="Razon Social"
              variant="outlined"
              value={cliente === null ? "" : cliente.RazonSocial}
            />
          </Box>
          <Box display="flex" justifyContent="center" m={1} p={1}>
            <TextField
              className={clases.texto}
              onChange={(e) => {
                accion(e.target.value, cliente.RazonSocial,cliente.SituacionIva,cliente.Observacion, cliente.Nuevo, 0);
              }}
              type="number"
              label="CUIT"
              variant="outlined"
              value={cliente === null ? "" : cliente.Cuit}
            />
          </Box>
          <Box display="flex" m={1} p={1}>
            <Select
              className={clases.texto}
              onChange={(value) =>
                accion(cliente.Cuit, cliente.RazonSocial, value.value,cliente.Observacion, cliente.Nuevo, 0)
              }
              placeholder={"Seleccionar Situacion"}
              noOptionsMessage={() => {
                return "No Se Encontraron Resultados";
              }}
              options={situacionIva}
              defaultValue={
                cliente === null
                  ? null
                  : situacionIva.filter(
                      (e) => e.value === cliente.SituacionIva
                    )[0]
              }
            />
          </Box>
          <Box display="flex" justifyContent="center" m={1} p={1}>
            <TextField
              className={clases.texto}
              onChange={(e) => {
                accion(cliente.Cuit, cliente.RazonSocial, cliente.SituacionIva,e.target.value, cliente.Nuevo, 0);
              }}
              rows={3}
              type="text"
              multiline
              label="Observacion"
              variant="outlined"
              value={cliente === null ? "" : cliente.Observacion}
            />
          </Box>
        </form>
      )}
    </>
  );
};

export default SeleccionCliente;
