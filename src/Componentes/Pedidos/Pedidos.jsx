import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import CargaPedido from "./CargaPedido";
import Alerta from "../Layout/Alerta";
import { useSelector, useDispatch } from "react-redux";
import {
  borrarErrorAccion,
  obtenerPedidosAccion,
} from "../../Redux/PedidoDucks";
import Tabla from "../Layout/Tabla";
import Slide from "@material-ui/core/Slide";
import { Button, Dialog } from "@material-ui/core";


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});


const Pedidos = () => {
  const dispatch = useDispatch();
  
  const [open, setOpen] = React.useState(false);
  const [pedido,setPedido] = useState(null)
  const error = useSelector((store) => store.pedido.error);
  const pedidos = useSelector((store) => store.pedido.array);

  const handleClickOpen = (e) => {
    setPedido(null);
    setOpen(true);
  };

  const handleClose = (e) => {
    //setPedido(null);
    setOpen(false);
  };


  const columnas = [
    { title: "Razon Social", field: "RazonSocial" },
    { title: "Cuit", field: "Cuit" },
    { title: "Fecha", field: "Fecha" },
  ];
  const columnasInicial = [
    { title: "Razon Social", field: "RazonSocial" },
    { title: "Cuit", field: "Cuit" },
    { title: "Fecha", field: "Fecha" },
  ];

  React.useEffect(() => {
    const obtenerInfo = () => {
      dispatch(obtenerPedidosAccion());
    };
    obtenerInfo();
  }, [dispatch]);

  const editarPedido = (datos) => {
    setPedido(datos);
    setOpen(true);
  };
  return (
    <>
      <Typography variant="h4">Pedidos</Typography>
      {error.length > 0
        ? error.map((e) => (
            <Alerta
              key={e.mensaje}
              mensaje={e.mensaje}
              tipo={e.tipo}
              accion={borrarErrorAccion}
            />
          ))
        : null}
        <Button
        style={{ marginBottom: 10, marginTop: 10 }}
        variant="outlined"
        color="primary"
        onClick={handleClickOpen}
      >
        Realizar Pedido
      </Button>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
      <CargaPedido pedido={pedido} cerrar={() => handleClose()} />
      </Dialog>
      <Tabla
        onRowClick={editarPedido}
        seleccionColumnas={false}
        title={""}
        data={pedidos}
        columns={columnas}
        see={columnasInicial}
        options={{
          grouping: true,
          filtering: true,
        }}
      />
    </>
  );
};

export default Pedidos;
