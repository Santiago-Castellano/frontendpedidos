import React from "react";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import { makeStyles } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
import DeleteOutlineIcon from "@material-ui/icons/DeleteOutline";
import EditIcon from "@material-ui/icons/Edit";
import TextField from "@material-ui/core/TextField";

import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  texto: {
    width: "100%",
  },
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    position: "relative",
    overflow: "auto",
    maxHeight: 300,
  },
  listSection: {
    backgroundColor: "inherit",
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0,
  },
}));

const ResumenPedido = ({
  cliente,
  carrito,
  seleccionarItem,
  quitarDelCarrito,
  agregaObservacion,
  Observacion,
}) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [item, setItem] = React.useState(null);
  const obtenerLineas = () => {
    const lineas = [];
    carrito.forEach((item) => {
      let linea = lineas.find((l) => l.nombre === item.Producto.Linea);
      if (linea) {
        linea.items.push(item);
      } else {
        lineas.push({
          nombre: item.Producto.Linea,
          items: [item],
        });
      }
    });
    return lineas;
  };

  const handleClickOpen = (item) => {
    setItem(item);
    setOpen(true);
  };

  const quitar = () => {
    let id = item.Id;
    setItem(null);
    setOpen(false);
    quitarDelCarrito(id);
  };
  const editarItem = () => {
    let pasar = item;
    setItem(null);
    setOpen(false);
    seleccionarItem(pasar);
  };
  return (
    <>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth={true}
      >
        <DialogTitle id="alert-dialog-title">
          {item ? item.Producto.Descripcion : null}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <Box display="flex" p={1}>
              <Grid xs={4}>
                <Typography variant="h6" align="left">
                  Cantidad:
                </Typography>
              </Grid>
              <Grid xs={8}>
                <Typography variant="h6" align="right">
                  {item ? item.Cantidad : null}
                </Typography>
              </Grid>
            </Box>
            <Box display="flex" p={1}>
              <Grid xs={4}>
                <Typography variant="h6" align="left">
                  Kilos:
                </Typography>
              </Grid>
              <Grid xs={8}>
                <Typography variant="h6" align="right">
                  {item ? item.Kilos : null}
                </Typography>
              </Grid>
            </Box>
            <Box display="flex" p={1}>
              <Grid xs={4}>
                <Typography variant="h6" align="left">
                  Precio:
                </Typography>
              </Grid>
              <Grid xs={8}>
                <Typography variant="h6" align="right">
                  $ {item ? parseFloat(item.Precio).toFixed(2) : null}
                </Typography>
              </Grid>
            </Box>
            <Box display="flex" p={1}>
              <Grid xs={4}>
                <Typography variant="h6" align="left">
                  Total:
                </Typography>
              </Grid>
              <Grid xs={8}>
                <Typography variant="h6" align="right">
                  $ {item ? item.Total : null}
                </Typography>
              </Grid>
            </Box>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            variant="outlined"
            startIcon={<DeleteOutlineIcon />}
            onClick={quitar}
            color="secondary"
          >
            Borrar
          </Button>
          <Button
            startIcon={<EditIcon />}
            variant="outlined"
            onClick={editarItem}
            color="primary"
          >
            Editar
          </Button>
        </DialogActions>
      </Dialog>
      <Box border={1} borderColor="primary" m={1} borderRadius={16}>
        <Box display="flex" justifyContent="center" p={1}>
          <Typography variant="h6">{cliente.RazonSocial}</Typography>
        </Box>
        <Box display="flex" justifyContent="center" p={1}>
          <Typography variant="h6">{cliente.Cuit}</Typography>
        </Box>
      </Box>
      <Box display="flex" m={1} p={1}>
        <List className={classes.root} subheader={<li />}>
          {obtenerLineas().map((linea) => (
            <li key={`section-${linea.nombre}`} className={classes.listSection}>
              <ul className={classes.ul}>
                <ListSubheader color="primary">{`Linea ${linea.nombre}`}</ListSubheader>
                {linea.items.map((item) => (
                  <ListItem
                    onClick={() => handleClickOpen(item)}
                    key={`${item.Id}`}
                  >
                    <ListItemText
                      multiline
                      primary={`${item.Producto.Codigo} - ${item.Producto.Descripcion}`}
                      secondary={
                        <>
                          <Box display="flex" justifyContent="left">
                            <Typography>
                              Unidades: {item.Cantidad} - Kilos: {item.Kilos}
                            </Typography>
                          </Box>
                          <Box display="flex">
                            <Typography>
                              Precio: ${item.Precio} -  ${item.Producto.UnidadMedida ==="K" ? "Kilos" : "Unidades"}  * Precio: ${item.Total}
                            </Typography>
                          </Box>
                        </>
                      }
                    />
                  </ListItem>
                ))}
              </ul>
            </li>
          ))}
        </List>
      </Box>
      <Box display="flex" justifyContent="center" p={1}>
        <TextField
          className={classes.texto}
          onChange={(e) => {
            agregaObservacion(e.target.value);
          }}
          rows={3}
          type="text"
          multiline
          label="Observacion"
          variant="outlined"
          value={Observacion === null ? "" : Observacion}
        />
      </Box>
    </>
  );
};

export default ResumenPedido;
