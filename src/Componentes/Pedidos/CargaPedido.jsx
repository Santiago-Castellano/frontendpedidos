import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import Stepper from "@material-ui/core/Stepper";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import SeleccionCliente from "./SeleccionCliente";
import MuestraProductos from "./MuestraProductos";
import SeleccionItem from "./SeleccionItem";
import ShoppingCartOutlinedIcon from "@material-ui/icons/ShoppingCartOutlined";
import ResumenPedido from "./ResumenPedido";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import { Box } from "@material-ui/core";
import { obtenerProductoAccion } from "../../Redux/ProductoDucks";
import { useDispatch } from "react-redux";
import { agregarPedidoAccion } from "../../Redux/PedidoDucks";
import { cargandoSitioAccion } from "../../Redux/UsuarioDucks";

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: "relative",
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(1),
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ["Cliente", "Carrito", "Confirmacion"];
}

export default function CargaPedido({ pedido, cerrar }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [activeStep, setActiveStep] = React.useState(0);
  const [carrito, setCarrito] = React.useState([]);
  const [item, setItem] = React.useState(null);
  const [observacion, setObservacion] = React.useState("");
  const [cliente, setCliente] = React.useState({
    Cuit: pedido ? pedido.Cuit : "",
    RazonSocial: pedido ? pedido.RazonSocial : "",
    SituacionIva: "",
    Observacion: "",
    Nuevo: false,
    Id: pedido ? pedido.IdCliente : 0,
  });
  const steps = getSteps();

  React.useEffect(() => {
    const obtenerInfo = () => {
      setCarrito([]);
      dispatch(obtenerProductoAccion(cliente.Id));
    };
    if (cliente.Id !== 0) {
      obtenerInfo();
    }
  }, [cliente, dispatch]);

  React.useEffect(() => {
    if(pedido !== null){
      setCarrito(pedido.Detalles);
    }
  }, [pedido]);

  const handleNext = () => {
    if (activeStep === 2) {
      dispatch(cargandoSitioAccion(true));
      dispatch(
        agregarPedidoAccion(
          { cliente: cliente, observacion, Id: pedido ? pedido.Id : 0 },
          carrito.map((i) => {
            return {
              Cantidad: i.Cantidad,
              Kilos: i.Kilos,
              Precio: i.Precio,
              DescripcionPrecio: i.DescripcionPrecio,
              IdProducto: i.Producto.Id,
            };
          })
        )
      );
      cerrarPedido();
    } else {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const actualizarCliente = (
    cuit,
    razonSocial,
    situacionIva,
    observacion,
    nuevo,
    id
  ) => {
    setCliente({
      Cuit: cuit,
      RazonSocial: razonSocial,
      SituacionIva: situacionIva,
      Observacion: observacion,
      Nuevo: nuevo,
      Id: id,
    });
  };

  const controlAvanzar = () => {
    switch (activeStep) {
      case 0:
        return cliente.Cuit !== "" && cliente.RazonSocial !== "";
      case 1:
        return carrito.length !== 0;
      case 2:
        return carrito.length !== 0;
      default:
        return false;
    }
  };
  const mostrarComponente = () => {
    switch (activeStep) {
      case 0:
        return (
          <SeleccionCliente accion={actualizarCliente} cliente={cliente} />
        );
      case 1:
        return item === null ? (
          <MuestraProductos
            seleccionarProducto={(prod) =>
              setItem({
                Id: 0,
                Cantidad: 0,
                Kilos: 0,
                Total: 0,
                Precio: 0,
                Producto: prod,
                PrecioNuevo: false,
                DescripcionPrecio: "",
              })
            }
          />
        ) : (
          <SeleccionItem
            ParamItem={item}
            volver={() => setItem(null)}
            agregar={agregarAlCarrito}
          />
        );
      case 2:
        return (
          <ResumenPedido
            cliente={cliente}
            carrito={carrito}
            agregaObservacion={(o) => setObservacion(o)}
            Observacion={observacion}
            seleccionarItem={(item) => {
              setItem(item);
              setActiveStep(1);
            }}
            quitarDelCarrito={(id) => quitarDelCarrito(id)}
          />
        );
      default:
        return null;
    }
  };
  const obtenerTextoBotonAvanzar = () => {
    switch (activeStep) {
      case 0:
        return "Siguiente";
      case 1:
        return "Siguiente";
      case 2:
        let total = 0;
        carrito.forEach((item) => {
          total += item.Total;
        });
        return `Finalizar Pedido $ ${total}`;
      default:
        return "";
    }
  };
  const agregarAlCarrito = (item) => {
    if (item.Id === 0) {
      let max = 0;
      carrito.forEach((e) => {
        if (e.Id > max) {
          max = e.Id;
        }
      });
      item.Id = max + 1;
      setCarrito([...carrito, item]);
    } else {
      setCarrito([...carrito.filter((c) => c.Id !== item.Id), item]);
    }
    setItem(null);
  };

  const quitarDelCarrito = (id) => {
    let nuevo = [...carrito.filter((c) => c.Id !== id)];
    setCarrito(nuevo);
    if (nuevo.length === 0) {
      setActiveStep(1);
      setItem(null);
    }
  };
  const cerrarPedido = () => {
    setCliente({
      Cuit: "",
      RazonSocial: "",
      SituacionIva: "",
      Nuevo: false,
    });
    setCarrito([]);
    setActiveStep(0);
    cerrar();
  };
  return (
    <div>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            onClick={cerrarPedido}
            aria-label="close"
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" className={classes.title}>
            Pedido
          </Typography>
          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="center"
          >
            <Typography variant="h6">{carrito.length}</Typography>
            <ShoppingCartOutlinedIcon />
          </Grid>
        </Toolbar>
      </AppBar>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      {mostrarComponente()}
      <Box display="flex" justifyContent="center" m={1} p={1}>
        {item === null ? (
          <>
            {activeStep !== 0 ? (
              <Button
                variant="outlined"
                onClick={handleBack}
                className={classes.backButton}
                startIcon={<ArrowBackIosIcon />}
              >
                Volver
              </Button>
            ) : null}

            {controlAvanzar() ? (
              <Button
                variant="outlined"
                color="primary"
                endIcon={
                  obtenerTextoBotonAvanzar() === "Siguiente" ? (
                    <ArrowForwardIosIcon />
                  ) : null
                }
                onClick={handleNext}
              >
                {obtenerTextoBotonAvanzar()}
              </Button>
            ) : (
              <Button
                variant="outlined"
                color="primary"
                onClick={handleNext}
                disabled
                endIcon={
                  obtenerTextoBotonAvanzar() === "Siguiente" ? (
                    <ArrowForwardIosIcon />
                  ) : null
                }
              >
                {obtenerTextoBotonAvanzar()}
              </Button>
            )}
          </>
        ) : null}
      </Box>
    </div>
  );
}
