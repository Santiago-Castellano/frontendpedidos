import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { obtenerProductoAccion } from "../../Redux/ProductoDucks";

import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListSubheader from "@material-ui/core/ListSubheader";
import InputAdornment from "@material-ui/core/InputAdornment";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import SearchIcon from "@material-ui/icons/Search";
const useStyles = makeStyles((theme) => ({
  texto: {
    width: "100%",
  },
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    position: "relative",
    overflow: "auto",
    maxHeight: 300,
  },
  listSection: {
    backgroundColor: "inherit",
  },
  ul: {
    backgroundColor: "inherit",
    padding: 0,
  },
}));

const MuestraProductos = ({ seleccionarProducto }) => {
  const productos = useSelector((state) => state.producto.array);
  const classes = useStyles();
  const dispatch = useDispatch();
  const [filtro, setFiltro] = React.useState("");

  React.useEffect(() => {
    const obtenerInfo = () => {
      if (productos.length === 0) {
        dispatch(obtenerProductoAccion());
      }
    };
    obtenerInfo();
  });

  const obtenerLineas = () => {
    const lineas = [];
    filtroProductos().forEach((producto) => {
      let linea = lineas.find((l) => l.nombre === producto.Linea);
      if (linea) {
        linea.productos.push(producto);
      } else {
        lineas.push({
          nombre: producto.Linea,
          productos: [producto],
        });
      }
    });
    return lineas;
  };
  const filtroProductos = () => {
    if (filtro !== "") {
      return productos.filter(
        (p) =>
          p.Codigo.match(filtro) ||
          p.Descripcion.toLowerCase().match(filtro.toLowerCase())
      );
    } else {
      return productos;
    }
  };

  return (
    <div>
      <Box display="flex" justifyContent="center" m={1} p={1}>
        <OutlinedInput
          className={classes.texto}
          onChange={(e) => setFiltro(e.target.value)}
          placeholder="Codigo o Descripcion del producto"
          type="search"
          endAdornment={
            <InputAdornment>
              {filtro === "" ? <SearchIcon color="primary" /> : null}
            </InputAdornment>
          }
        />
      </Box>
      <Box display="flex" m={1} p={1}>
        <List className={classes.root} subheader={<li />}>
          {obtenerLineas().map((linea) => (
            <li key={`section-${linea.nombre}`} className={classes.listSection}>
              <ul className={classes.ul}>
                <ListSubheader color="primary">{`Linea ${linea.nombre}`}</ListSubheader>
                {linea.productos.map((producto) => (
                  <ListItem
                    onClick={() => seleccionarProducto(producto)}
                    key={`item-${linea.nombre}-${producto.Codigo}`}
                  >
                    <ListItemText
                      primary={`${producto.Codigo} - ${producto.Descripcion}`}
                      secondary={`$ ${parseFloat(producto.Precio).toFixed(2)}`}
                    />
                  </ListItem>
                ))}
              </ul>
            </li>
          ))}
        </List>
      </Box>
    </div>
  );
};

export default MuestraProductos;
