import React from "react";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Select from "react-select";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import ArrowBackIosIcon from "@material-ui/icons/ArrowBackIos";

const useStyles = makeStyles((theme) => ({
  backButton: {
    marginRight: theme.spacing(1),
  },
  texto: {
    width: "100%",
  },
}));

const SeleccionItem = ({ ParamItem, volver, agregar }) => {
  const [item, setItem] = React.useState(ParamItem);
  const classes = useStyles();

  const handleChange = (event) => {
    setItem({ ...item, PrecioNuevo: event.target.checked, Precio: 0 });
  };
  const agregarAlCarrito = () => {
    const pas = item;
    pas.Total =
      item.Producto.UnidadMedida === "U"
        ?  item.Cantidad * item.Precio
        :  item.Kilos * item.Precio;
    pas.Total = parseFloat(pas.Total).toFixed(2);
    pas.Cantidad = parseFloat(pas.Cantidad).toFixed(2);
    pas.Kilos = parseFloat(pas.Kilos).toFixed(2);

    agregar(pas);
  };
  const puedeAgregar = () => {
    if (item.Producto.UnidadMedida === "U") {
      return !(item.Precio > 0 && item.Cantidad > 0);
    } else {
      return !(item.Precio > 0 && item.Kilos > 0);
    }
  };
  return (
    <>
      <Box border={1} borderColor="primary" m={1} borderRadius={16}>
        <Box display="flex" justifyContent="center" p={1}>
          {item.Producto.Codigo} - {item.Producto.Descripcion}
        </Box>
        <Box display="flex" justifyContent="center" p={1}>
          Cantidad en Stock {item.Producto.StockCantidad}
        </Box>
        <Box display="flex" justifyContent="center" p={1}>
          Kilos en Stock {item.Producto.StockKilos}
        </Box>
      </Box>
      <form autoComplete="off" onSubmit={agregarAlCarrito}>
        <Box display="flex" justifyContent="center" m={1} p={1}>
          <TextField
            className={classes.texto}
            inputProps={{ style: { textAlign: "right" } }}
            onChange={(e) => {
              setItem({ ...item, Cantidad: e.target.value });
            }}
            label="Cantidad"
            type="number"
            variant="outlined"
            value={item.Cantidad === 0 ? "" : item.Cantidad}
            required={item.Producto.UnidadMedida === "U"}
          />
        </Box>
        <Box display="flex" justifyContent="center" m={1} p={1}>
          <TextField
            className={classes.texto}
            onChange={(e) => {
              setItem({ ...item, Kilos: e.target.value });
            }}
            inputProps={{ style: { textAlign: "right" } }}
            type="number"
            label="Kilos"
            variant="outlined"
            value={item.Kilos === 0 ? "" : item.Kilos}
            required={item.Producto.UnidadMedida === "Kg"}
          />
        </Box>
        {item.PrecioNuevo === false ? (
          <Box display="flex" justifyContent="center" m={1} p={1}>
            <Select
              textAlign={"right"}
              className={classes.texto}
              placeholder={"Seleccionar Precio"}
              getOptionLabel={(option) =>
                `${option.Precio} - ${option.Descripcion}`
              }
              getOptionValue={(option) => option["Precio"]}
              options={item.Producto.Precios}
              onChange={(value) => {
                setItem({
                  ...item,
                  Precio: value.Precio,
                  DescripcionPrecio: value.Descripcion,
                });
              }}
              noOptionsMessage={() => {
                return "No Se Encontraron Resultados";
              }}
              defaultValue={
                item === null
                  ? null
                  : item.Producto.Precios.filter(
                      (p) => p.Precio === item.Precio
                    )[0]
              }
            />
          </Box>
        ) : (
          <Box display="flex" justifyContent="center" m={1} p={1}>
            <TextField
              className={classes.texto}
              onChange={(e) => {
                setItem({
                  ...item,
                  Precio: e.target.value,
                  PrecioNuevo: true,
                  DescripcionPrecio: "Precio Nuevo",
                });
              }}
              type="number"
              label="Precio"
              inputProps={{ style: { textAlign: "right" } }}
              variant="outlined"
              value={item.Precio === 0 ? "" : item.Precio}
              required={true}
            />
          </Box>
        )}

        <Box display="flex" m={0} p={1}>
          <FormControlLabel
            control={
              <Checkbox
                checked={item === null ? false : item.PrecioNuevo}
                onChange={handleChange}
                name="PrecioNuevo"
                color="primary"
                label="Precio Nuevo"
              />
            }
            label="Precio Nuevo"
          />
        </Box>

        <Box display="flex" justifyContent="center" m={1} p={1}>
          <Button
            variant="outlined"
            onClick={volver}
            className={classes.backButton}
            startIcon={<ArrowBackIosIcon />}
          >
            Volver
          </Button>
          <Button
            disabled={puedeAgregar()}
            variant="outlined"
            color="primary"
            type="submit"
          >
            Aceptar ${" "}
            {item.Producto.UnidadMedida === "U"
              ? parseFloat(item.Cantidad * item.Precio).toFixed(2)
              : parseFloat(item.Kilos * item.Precio).toFixed(2) }
          </Button>
        </Box>
      </form>
    </>
  );
};

export default SeleccionItem;
