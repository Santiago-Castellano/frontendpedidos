import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import { Link, Redirect } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { withRouter } from "react-router-dom";
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { useDispatch, useSelector } from "react-redux";
import {
  iniciarSesionAccion,
  borrarErrorAccion,
  cargandoSitioAccion,
} from "../../Redux/UsuarioDucks";
import { useState } from "react";
import Copyright from "../Layout/Copyright";
import { registro, inicio } from "../../Configuracion/Rutas";
import Alerta from "../Layout/Alerta";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login = (props) => {
  const classes = useStyles();
  const usuario = useSelector((store) => store.usuario);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [mostrarPass, setMostrarPass] = useState(false);
  const dispatch = useDispatch();
  const enviarDatos = (e) => {
    e.preventDefault();
    dispatch(cargandoSitioAccion());
    dispatch(iniciarSesionAccion(username, password));
  };

  return (
    <Container component="main" maxWidth="xs">
      {usuario.logeado ? <Redirect to={inicio()} /> : null}
      {usuario.error.length > 0
        ? usuario.error.map((e) => (
            <Alerta
              key={e.mensaje}
              mensaje={e.mensaje}
              tipo={e.tipo}
              accion={borrarErrorAccion}
            />
          ))
        : null}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Iniciar Sesión
        </Typography>
        <form className={classes.form} onSubmit={enviarDatos}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="username"
            label="Usuario"
            name="username"
            autoFocus
            onChange={(e) => setUsername(e.target.value)}
          />
          <OutlinedInput
            required
            fullWidth
            name="password"
            placeholder="Contraseña"
            type={mostrarPass ? 'text' : 'password'}
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target.value)}
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={() => setMostrarPass(!mostrarPass)}
                >
                  {mostrarPass ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            }
          />
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
          >
            Iniciar Sesión
          </Button>
          <Grid container>
            <Grid item>
              <Link to={registro()} variant="body2">
                {"Registrarse"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default withRouter(Login);
