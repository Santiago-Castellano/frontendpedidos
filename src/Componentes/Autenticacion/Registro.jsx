import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { useState } from "react";
import { withRouter } from "react-router-dom";
import Copyright from "../Layout/Copyright";

import { registroAccion, borrarErrorAccion } from "../../Redux/UsuarioDucks";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../Configuracion/Rutas";
import Alerta from "../Layout/Alerta";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Registro = (props) => {
  const classes = useStyles();
  const [usuario, setUsuario] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password_2, setPassword_2] = useState("");
  const error = useSelector((store) => store.usuario.error);

  const dispatch = useDispatch();
  React.useEffect(() => {
    const redireccionar = () => {
      let redireccion = error.filter((er) => er.tipo === "success");
      if (redireccion.length === 1) {
        props.history.push(login());
      }
    };
    redireccionar();
  }, [error,props.history]);

  const enviarDatos = (e) => {
    e.preventDefault();
    dispatch(registroAccion(usuario, password, password_2, email));
  };
  return (
    <Container component="main" maxWidth="xs">
      {error.length > 0
        ? error.map((e) => (
            <Alerta
              key={e.mensaje}
              mensaje={e.mensaje}
              tipo={e.tipo}
              accion={borrarErrorAccion}
            />
          ))
        : null}
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Registrarse
        </Typography>
        <form className={classes.form} onSubmit={enviarDatos}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="usuario"
                label="Usuario"
                name="usuario"
                onChange={(e) => setUsuario(e.target.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                onChange={(e) => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Contraseña"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password_2"
                label="Confirmar Contraseña"
                type="password"
                id="password_2"
                autoComplete="current-password"
                onChange={(e) => setPassword_2(e.target.value)}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="outlined"
            color="primary"
            className={classes.submit}
          >
            Registrarse
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link to={login()} variant="body2">
                ¿Ya estas registrado?, Inicia Sesíon
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
};

export default withRouter(Registro);
