import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import GetAppIcon from "@material-ui/icons/GetApp";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import Input from "@material-ui/core/Input";

import { useDispatch, useSelector } from "react-redux";

import {
  obtenerDatosAccion,
  importarAccion,
  cargarndoAccion,
  exportarAccion,
} from "../Redux/SincronizacionDucks";
import { LinearProgress } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  box: {
    margin: theme.spacing(1),
  },
  button: {
    margin: theme.spacing(1),
  },
}));

const Sincronizacion = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const datos = useSelector((store) => store.sincronizacion.array);
  const [open, setOpen] = React.useState(false);
  const [modelo, setModelo] = React.useState("");
  React.useEffect(() => {
    const obtenerInfo = () => {
      dispatch(obtenerDatosAccion());
    };
    obtenerInfo();
  }, [dispatch]);

  const handleClickAbrir = (modelo) => {
    setOpen(true);
    setModelo(modelo);
  };

  const handleCerrar = () => {
    setOpen(false);
  };
  const enviarDatos = (e) => {
    e.preventDefault();
    const form = new FormData(e.currentTarget);
    dispatch(cargarndoAccion(modelo, true));
    dispatch(importarAccion(form, modelo));
    handleCerrar();
  };
  return (
    <>
      <Typography variant="h4">Sincronizacion</Typography>
      <Grid
        container
        direction="row"
        justify="space-evenly"
        alignItems="center"
      >
        {datos.map((item) => (
          <Box key={item.Id} className={classes.box}>
            <Card variant="outlined">
              <CardContent>
                <Typography variant="h5">{item.NombreModelo}</Typography>
                <br />
                {item.TieneImportacion ? (
                  <Typography color="textSecondary">
                    Importacion: {item.Importacion}
                    {item.Importacion === "Importando" ? (
                      <LinearProgress />
                    ) : null}
                  </Typography>
                ) : null}
                {item.TieneExportacion ? (
                  <>
                    {item.TieneImportacion ? <br /> : null}
                    <Typography color="textSecondary">
                      Exportacion: {item.Exportacion}
                    </Typography>
                    {item.Exportacion === "Exportando" ? (
                      <LinearProgress color="secondary" />
                    ) : null}
                  </>
                ) : null}
              </CardContent>
              <CardActions>
                {item.TieneImportacion ? (
                  <Button
                    variant="outlined"
                    disabled={item.Importacion === "Importando"}
                    color="primary"
                    className={classes.button}
                    startIcon={<CloudUploadIcon />}
                    onClick={() => handleClickAbrir(item.NombreModelo)}
                  >
                    Importar
                  </Button>
                ) : null}
                {item.TieneExportacion ? (
                  <Button
                    variant="outlined"
                    color="secondary"
                    className={classes.button}
                    startIcon={<GetAppIcon />}
                    disabled={item.Exportacion === "Exportando"}
                    onClick={() => {
                      dispatch(cargarndoAccion(item.NombreModelo, false));
                      dispatch(exportarAccion(item.NombreModelo));
                    }}
                  >
                    Exportar
                  </Button>
                ) : null}
              </CardActions>
            </Card>
          </Box>
        ))}
      </Grid>
      <Dialog
        open={open}
        onClose={handleCerrar}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Importacion</DialogTitle>
        <DialogContent>
          <div>
            <div>
              <Typography variant={"h5"}>
                Importar los datos de {modelo}
              </Typography>
              <br />
              <form onSubmit={enviarDatos}>
                <Input type="file" name="archivo"></Input>
                <Button
                  className={classes.button}
                  variant="outlined"
                  color="primary"
                  autoFocus
                  type="submit"
                >
                  Enviar
                </Button>
              </form>
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleCerrar}
            className={classes.button}
            variant="outlined"
            color="secondary"
          >
            Cerrar
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default Sincronizacion;
