import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { clientes, obtenerRoles, pedidos, productos, sincronizacion } from "../Configuracion/Rutas";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

const Inicio = () => {
  const classes = useStyles();
  const usuario = useSelector((store) => store.usuario);

  return (
    <>
      <Grid container spacing={4}>

        {obtenerRoles(pedidos()).includes(usuario.rol) ? (
          <Grid item xs={12}>
            <Link
              style={{ textDecoration: "none", color: "black" }}
              to={pedidos()}
            >
              <Card className={classes.root}>
                <CardActionArea>
                  <CardContent>
                    <Typography
                      gutterBottom
                      align="center"
                      variant="h5"
                      color="primary"
                      component="h2"
                    >
                      Pedidos
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>
        ) : null}
        {obtenerRoles(clientes()).includes(usuario.rol) ? (
          <Grid item xs={12}>
            <Link
              style={{ textDecoration: "none", color: "black" }}
              to={clientes()}
            >
              <Card className={classes.root}>
                <CardActionArea>
                  <CardContent>
                    <Typography
                      gutterBottom
                      align="center"
                      variant="h5"
                      component="h2"
                      color="primary"

                    >
                      Clientes
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>
        ) : null}
        {obtenerRoles(productos()).includes(usuario.rol) ? (
          <Grid item xs={12}>
            <Link
              style={{ textDecoration: "none", color: "black" }}
              to={productos()}
            >
              <Card className={classes.root}>
                <CardActionArea>
                  <CardContent>
                    <Typography
                      gutterBottom
                      align="center"
                      variant="h5"
                      component="h2"
                      color="primary"
                    >
                      Productos
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>
        ) : null}
        {obtenerRoles(sincronizacion()).includes(usuario.rol) ? (
          <Grid item xs={12}>
            <Link
              style={{ textDecoration: "none", color: "black" }}
              to={sincronizacion()}
            >
              <Card className={classes.root}>
                <CardActionArea>
                  <CardContent>
                    <Typography
                      gutterBottom
                      align="center"
                      variant="h5"
                      color="primary"
                      component="h2"
                    >
                      Sincronizacion
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Link>
          </Grid>
        ) : null}
      </Grid>
    </>
  );
};

export default Inicio;
