import React from "react";
import Typography from "@material-ui/core/Typography";
import { useDispatch, useSelector } from "react-redux";
import { obtenerProductoAccion } from "../Redux/ProductoDucks";
import Tabla from "./Layout/Tabla";

const Productos = () => {
  const dispatch = useDispatch();
  const productos = useSelector((store) => store.producto.array);
  const columnas = [
    { title: "Codigo", field: "Codigo" },
    { title: "Descripciond", field: "Descripcion" },
    { title: "Linea", field: "Linea" },
    { title: "Rubro", field: "Rubro" },
    { title: "Unidad Medida", field: "UnidadMedida" },
    { title: "Peso Promedio", field: "PesoPromedio",render: rowData => parseFloat(rowData.PesoPromedio).toFixed(2) },
    { title: "Cantidad", field: "StockCantidad",render: rowData => parseFloat(rowData.StockCantidad).toFixed(2) },
    { title: "Kilos", field: "StockKilos",render: rowData => parseFloat(rowData.StockKilos).toFixed(3) },
    { title: "Precio", field: "Precio",render: rowData => parseFloat(rowData.Precio).toFixed(2) },
  ];
  const columnasIniciales = [
    { title: "Codigo", field: "Codigo" },
    { title: "Descripciond", field: "Descripcion" },
    { title: "Cantidad", field: "StockCantidad",render: rowData => parseFloat(rowData.StockCantidad).toFixed(2) },
    { title: "Kilos", field: "StockKilos",render: rowData => parseFloat(rowData.StockKilos).toFixed(3) },
    { title: "Precio", field: "Precio",render: rowData => parseFloat(rowData.Precio).toFixed(2) },
  ];
  React.useEffect(() => {
    const obtenerInfo = () => {
      dispatch(obtenerProductoAccion());
    };
    obtenerInfo();
  }, [dispatch]);

  return (
    <div>
      <Typography variant="h4">Productos</Typography>
      <Tabla
        seleccionColumnas={true}
        title={""}
        data={productos}
        columns={columnas}
        see={columnasIniciales}
        options={{
          grouping: true,
        }}
      />
    </div>
  );
};

export default Productos;
