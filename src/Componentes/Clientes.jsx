import React from "react";
import Typography from "@material-ui/core/Typography";

import { useDispatch, useSelector } from "react-redux";

import { obtenerClientesAccion } from "../Redux/ClienteDucks";
import Tabla from "./Layout/Tabla";

const Clientes = () => {
  const dispatch = useDispatch();
  const clientes = useSelector((store) => store.clientes.array);
  const columnas = [
    { title: "Razon Social", field: "RazonSocial" },
    { title: "Observacion", field: "Observacion" },
    { title: "Localidad", field: "Localidad" },
    { title: "Direccion", field: "Direccion" },
    { title: "Telefono", field: "Telefono" },
  ];
  const columnasInicial = [
    { title: "Razon Social", field: "RazonSocial" },
    { title: "Telefono", field: "Telefono" },
  ];
  React.useEffect(() => {
    const obtenerInfo = () => {
      dispatch(obtenerClientesAccion());
    };
    obtenerInfo();
  }, [dispatch]);

  return (
    <>
      <Typography variant="h4">Clientes</Typography>
      <Tabla
        seleccionColumnas={true}
        title={""}
        data={clientes}
        columns={columnas}
        see={columnasInicial}
        options={{
          grouping: true,
          filtering: true,
        }}
      />
    </>
  );
};

export default Clientes;
