import React from "react";
import { Tabla } from "./Layout/Tabla";
import { useDispatch, useSelector } from "react-redux";
import {
  obtenerEmpresasAccion,
  agregarEmpresaAccion,
  borrarEmpresaAccion,
  editarEmpresaAccion,
  borrarErrorAccion
} from "../Redux/EmpresaDucks";
import { Typography } from "@material-ui/core";
import Alerta from "./Layout/Alerta";

const Empresas = () => {
  const dispatch = useDispatch();
  const datos = useSelector((store) => store.empresas.array);
  const error = useSelector((store) => store.empresas.error);
  const columnas = [{ title: "Nombre", field: "Nombre" }];

  React.useEffect(() => {
    const obtenerInfo = () => {
      dispatch(obtenerEmpresasAccion());
    };
    obtenerInfo();
  }, [dispatch]);

  const handleRowUpdate = (newData, oldData, resolve) => {
    const editar = () => {
      dispatch(editarEmpresaAccion(newData));
    };
    editar();
    resolve();
  };

  const handleRowAdd = (newData, resolve) => {
    const agregar = () => {
      dispatch(agregarEmpresaAccion(newData));
    };
    agregar();
    resolve();
  };
  const handleRowDelete = (oldData, resolve) => {
    const borrar = () => {
      dispatch(borrarEmpresaAccion(oldData));
    };
    borrar();
    resolve();
  };

  return (
    <>
      {error.length > 0
        ? error.map((e) => (
            <Alerta
              key={e.mensaje}
              mensaje={e.mensaje}
              tipo={e.tipo}
              accion={borrarErrorAccion}
            />
          ))
        : null}
      <Typography variant="h4">Empresas</Typography>
      <Tabla
        title={""}
        seleccionColumnas={false}
        columns={columnas}
        see={columnas}
        data={datos}
        editable={{
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve) => {
              handleRowUpdate(newData, oldData, resolve);
            }),
          onRowAdd: (newData) =>
            new Promise((resolve) => {
              handleRowAdd(newData, resolve);
            }),
          onRowDelete: (oldData) =>
            new Promise((resolve) => {
              handleRowDelete(oldData, resolve);
            }),
        }}
      />
    </>
  );
};

export default Empresas;
