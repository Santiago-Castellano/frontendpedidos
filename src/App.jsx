import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { useSelector } from "react-redux";

import {
  login,
  inicio,
  sincronizacion,
  productos,
  pedidos,
  clientes,
  registro,
  empresas,
  usuarios,
} from "./Configuracion/Rutas";
import Login from "./Componentes/Autenticacion/Login";
import Registro from "./Componentes/Autenticacion/Registro";
import Menu from "./Componentes/Layout/Menu";
import Clientes from "./Componentes/Clientes";
import Inicio from "./Componentes/Inicio";
import Pedidos from "./Componentes/Pedidos/Pedidos";
import Productos from "./Componentes/Productos";
import Sincronizacion from "./Componentes/Sincronizacion";
import Empresas from "./Componentes/Empresas";
import { obtenerRoles } from "./Configuracion/Rutas";
import Usuarios from "./Componentes/Usuarios";
import SeleccionarEmpresa from "./Componentes/Layout/SeleccionarEmpresa";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Grid } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: "100vh",
    overflow: "auto",
  },
  container: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
}));

function App() {
  const classes = useStyles();
  const usuario = useSelector((store) => store.usuario);

  const RutaProtegida = ({ roles, component, path, ...rest }) => {
    if (usuario.logeado) {
      if (roles.includes(usuario.rol)) {
        if (usuario.empresas.length > 1) {
          return <Route component={SeleccionarEmpresa} path={path} {...rest} />;
        }
        return <Route component={component} path={path} {...rest} />;
      } else {
        return <Redirect to={inicio()} {...rest} />;
      }
    } else {
      return <Redirect to={login()} {...rest} />;
    }
  };

  return (
    <Router>
      {usuario.cargando ? (
        <Grid
        style={{ padding:"25%"}}
          container
          direction="row"
          justify="space-evenly"
          alignItems="center"
        >
          <CircularProgress circle="true" size={100} />
          <Grid item style={{width:"100%", paddingTop:10}}>
          <Typography align="center" variant="h4" color="primary">Cargando</Typography>
          </Grid>
        </Grid>
      ) : (
        <div className={classes.root}>
          <Menu />
          <main className={classes.content}>
            <div className={classes.appBarSpacer} />
            <Container maxWidth="lg" className={classes.container}>
              <Switch>
                <Route exact path={registro()}>
                  <Registro />
                </Route>
                <Route exact path={login()}>
                  <Login />
                </Route>
                <RutaProtegida
                  roles={obtenerRoles(inicio())}
                  component={Inicio}
                  path={inicio()}
                  exact
                />
                <RutaProtegida
                  roles={obtenerRoles(sincronizacion())}
                  component={Sincronizacion}
                  path={sincronizacion()}
                  exact
                />
                <RutaProtegida
                  roles={obtenerRoles(usuarios())}
                  component={Usuarios}
                  path={usuarios()}
                  exact
                />
                <RutaProtegida
                  roles={obtenerRoles(pedidos())}
                  component={Pedidos}
                  path={pedidos()}
                  exact
                />
                <RutaProtegida
                  roles={obtenerRoles(clientes())}
                  component={Clientes}
                  path={clientes()}
                  exact
                />
                <RutaProtegida
                  roles={obtenerRoles(productos())}
                  component={Productos}
                  path={productos()}
                  exact
                />
                <RutaProtegida
                  roles={obtenerRoles(empresas())}
                  component={Empresas}
                  path={empresas()}
                  exact
                />

                <RutaProtegida
                  roles={obtenerRoles()}
                  component={null}
                  path="/"
                  exact
                />
              </Switch>
            </Container>
          </main>
        </div>
      )}
    </Router>
  );
}

export default App;
